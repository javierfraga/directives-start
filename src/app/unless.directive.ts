import { Directive,Input,TemplateRef,ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
	// this is just a re-creattion of the ngIf directive
	// but its opposite, shows if is false.
	@Input() set appUnless(condition:boolean) {
		if (!condition) {
			this.vcRef.createEmbeddedView(this.templateRef);
		} else {
			this.vcRef.clear();
		}
	}

	constructor(private templateRef:TemplateRef<any>, private vcRef:ViewContainerRef) { }

}
