import { Directive,OnInit,ElementRef,Renderer2,HostListener,HostBinding,Input } from '@angular/core';
// import { Directive,OnInit,ElementRef,RendererV2 } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective  implements OnInit{
  @Input() defaultColor:string = 'transparent';
  /*
   * Here we set the directive to pass value in directly thru directive in template
   */
  @Input('appBetterHighlight') highlightColor:string = 'blue';
  // notice this is causing it to be transparent onload
  /*
   * This is the best way to change the color of div
   */
  @HostBinding('style.backgroundColor') backgroundColor:string;
  // we can move it to the ngOnInit
  // @HostBinding('style.backgroundColor') backgroundColor:string=this.defaultColor

  /*
   * Not best practice to access element directly with ElementRef
   * So use Renderer2 instead
   */
  constructor(private elRef:ElementRef, private renderer:Renderer2) { }
  // constructor(private elRef:ElementRef, private renderer:RendererV2) { }

  ngOnInit() {
    // this resolved that bug with transparent coming up first on load
    this.backgroundColor = this.defaultColor;
  	// this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  }

  @HostListener('mouseenter') mouseover(eventData:Event) {
  	// this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  	// didn't work below. but max uses it?
    // this.backgroundColor = 'blue';
  	this.backgroundColor = this.highlightColor;
  }
  @HostListener('mouseleave') mouseleave(eventData:Event) {
  	// this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.backgroundColor = this.defaultColor;
  }
}
